-- 2. Create SQL Queries using Advanced Selects and Joining Tables to return the following:
	-- a. Find all artists that has letter D in its name
	SELECT * FROM artists WHERE name LIKE "%d%";

	-- b. Find all songs that has a length of less than 230
	SELECT * FROM songs WHERE length < 230;

	-- c. Join the 'albums' and 'songs' table. (Only show the album name, song name, and song length)
	SELECT album_title, song_name, length FROM albums
		JOIN songs ON albums.id  = songs.album_id; 

	-- d. Join the 'artists' and 'albums' table. (Find all albums that has letter A in its name)
	SELECT * FROM artists
		JOIN albums ON artists.id = albums.artist_id WHERE albums.album_title LIKE "%a%";

	-- e. Sort the albums in Z-A order. (Show only the first 4 records)
	SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

	-- f. Join the 'albums' and 'songs' tables. (Sort albums from Z-A and sort songs from A-Z)
	SELECT * FROM albums
		JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC, song_name ASC;